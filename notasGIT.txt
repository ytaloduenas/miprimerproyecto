COMANDOS CONSOLA / TERMINAL

VER [de detalle]
pwd - para saber donde estamos
ls - listar
ls -a muestra archivos ocultos
ls -r lista carpeta y contenido
ls -al lista con detalle
clear - limpiar pantalla
cd - ingresar a carpeta desde el directorio
cd .. - retroceder

CREACION
mkdir - para crear
touch - crear archivos

DELETE
rmdir - [nombre de carpeta] elimina solo carpeta vacia
rm -r [nombre de carpeta]   borra todo
rm [nombre de archivo] borra archivo

UPDATE
mv [NOMBRE ORIGINAL] [NOMBRE NUEVO] renombrar archivo 

GIT
usuario para la computadora
$ git config --global user.name 'Ytalo Duenas'
$ git config --global user.email 'ytaloduenas@gmail.com'

